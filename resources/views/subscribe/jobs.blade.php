@extends('oxygen::layouts.master-frontend-internal')

@section('internal-page-contents')
    <div class="container mt-5 pt-5 mb-5 pt-5">
        <div class="row justify-content-center mt-5 pt-5 mb-5 pt-5">
            <div class="col-md-6">
               <h1>
                   This is the Page where you Post Jobs
               </h1>
            </div>
            <div class="col-md-12 mt-5 mb-5">
                <div class="row">
                    @if(Auth::user()->subscription('default')->onGracePeriod())
                        <h3 class="mb-4">
                            Subscription is Canceled, will expire on {{ Auth::user()->subscription('default')
                            ->ends_at->format('dS M Y') }}
                        </h3>
                        <a href="{{ route('subscribe.index') }}" class="btn btn-success w-100">Subscribe Again</a>
                    @else
                        <div class="col-md-12">
                            <a href="{{ route('subscribe.cancel') }}" class="btn btn-danger w-100">Cancel Subscription</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
