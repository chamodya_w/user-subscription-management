<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Subscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        $now = Carbon::now();

        if ($user == null) {
            return redirect('/login');
        }

        if ($user && $user->isA('user')) {
            if (Auth::user()->subscribed('default') || Auth::user()->subscription('default')->onGracePeriod()) {
                // the user has a plan subscribe or a canceled and has some time left
            } else {
                // the user has no active plan
                return redirect('/subscribe');
            }
        }

        return $next($request);
    }
}
