<?php

namespace App\Http\Controllers;

use App\Entities\UserSubscriptions\UserSubscriptionsRepository;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserSubscriptionController extends Controller
{
    /**
     * Create new subscription page
     *
     * @return Response
     */
    public function index(): Response
    {
        $plans = app(UserSubscriptionsRepository::class)->plans();
        $user = Auth::user();

        return response()->view('subscribe.index', [
            'title'     => 'User Subscription',
            'user'      => $user,
            'intent'    => $user->createSetupIntent(),
            'plans'     => $plans
        ]);
    }

    /**
     * Create user subscription
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $user = Auth::user();
        $paymentMethod = $request->input('payment_method');

        $user->createOrGetStripeCustomer();
        $user->addPaymentMethod($paymentMethod);

        /**
         * get the user selected plan, currently only one
         * Plans are created in the stripe account
         */
        $plan = $request->input('plan');

        try {
            // create plan
            $user->newSubscription('default', $plan)->create($paymentMethod, [
                'email' => $user->email
            ]);
        } catch (Exception $exception) {
            return back()->with('error', 'Subscription Error. ' . $exception->getMessage());
        }

        return redirect('jobs');
    }

    /**
     * Page where create jobs and show after subscribe
     *
     * @return Response
     */
    public function createJobs(): Response
    {
        return response()->view('subscribe.jobs', [
            'title'     => 'Job Postings',
        ]);
    }

    public function cancel(): RedirectResponse
    {
        try {
            // cancel the default subscript and put user on grace period
            $user = Auth::user();
            $user->subscription('default')->cancel();
        } catch (Exception $exception) {
            return back()->with('error', 'Subscription cancel Error. ' . $exception->getMessage());
        }

        return back()->with('success', 'Subscription Canceled');
    }
}
