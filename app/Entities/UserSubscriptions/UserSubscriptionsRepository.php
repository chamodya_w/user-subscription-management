<?php

namespace App\Entities\UserSubscriptions;

use App\Entities\BaseRepository;
use Stripe\StripeClient;
use function config;

class UserSubscriptionsRepository extends BaseRepository
{
    public function plans(): array
    {
        $key = config('services.stripe.secret');
        $stripe = new StripeClient($key);

        $res = $stripe->plans->all();
        $plans = $res->data;

        foreach($plans as $plan) {
            $prod = $stripe->products->retrieve(
                $plan->product,[]
            );
            $plan->product = $prod;
        }

        return $plans;
    }
}
